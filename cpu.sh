#!/usr/bin/env bash
#OAR --array-param-file /udd/yicwang/src/igrida_test/parameterfile.data
#OAR -l /cpu=1, walltime=0:09:59
#OAR -O /temp_dd/igrida-fs1/yicwang/SCRATCH/out/test_%jobid%.output
#OAR -E /temp_dd/igrida-fs1/yicwang/SCRATCH/out/test_%jobid%.error

#patch to be aware of "module" inside a job
. /etc/profile.d/modules.sh

set -xv

EXECUTABLE=new_group_lasso.py

source ~/.virtualenv/py36/bin/activate

echo
echo "=============== RUN ${OAR_JOB_ID} ==============="
echo "Running ..."
python ${EXECUTABLE} $*
echo "Done"
